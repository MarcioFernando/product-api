# API PRODUCTS

## Instalação

1. Faça o clone deste projeto com `git clone https://MarcioFernando@bitbucket.org/MarcioFernando/product-api.git`
2. Entre na pasta do projeto e instale as dependências com `npm install`

## Iniciando a API
Antes de iniciar a API verifique os dados para a conexão que está no arquivo _database.js_ na pasta "src".

Na pasta raiz do projeto digite o seguinte comando para iniciar o servidor `npm start`.

## Testando a API

A API roda sobre o seguinte endereço `http://localhost:3000/api`.

### Adicionando

Para adicionar usando o Postman basta selecionar a opção _POST_ no endereço `http://localhost:3000/api/add` e logo baixo escolher a opção _Body_ e na sequência _raw_ e _JSON(application/json)_ em seguida clique em _Send_. Veja o exemplo a baixo usando uma requisição.

```json
{
    "name": "Nome do produto",
    "title": "Título do produto",
    "description": "Descrição do produto"
}
```
![](src/imgs/add.jpg)

### Listando todos

Para listar todos basta selecionar a opção _GET_ no endereço `http://localhost:3000/api/get_all`, em seguida clique em _Send_.

### Listando por ID

Para listar um único elemento basta selecionar a opção _GET_ no endereço `http://localhost:3000/api/get/ID`, passando um ID, em seguida clique em _Send_.

### Atualizando elemento
 
Para atualizar um elemento basta selecionar a opção _PUT_ no endereço `http://localhost:3000/api/update/ID` passando um ID, em seguida selecione a opção _Body_ e em seguência _x-www-form-urlencoded_ preenchendo os campos com os novos valores, em seguida clique me _Send_. Veja o exemplo a baixo. 
![](src/imgs/update.jpg)

### Removendo

Para remover um elemento basta selecionar a opção _DELETE_ no seguinte endereço `http://localhost:3000/api/delete/ID`, passando um ID, em seguida clique em _Send_.