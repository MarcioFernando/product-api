const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var Product = new Schema({
  name: {
    type: String
  },
  title: {
    type: String
  },  
  description: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
},{
    collection: 'products'
});

module.exports = mongoose.model('Product', Product);