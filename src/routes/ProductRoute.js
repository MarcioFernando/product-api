const express = require('express');
const router = express.Router();

const Product = require('../models/Product');

//Add
router.post('/add', (req, res) => {
    const product = new Product(req.body);
    product.save()
    .then(product => {
        res.status(200).send("Adicionado com sucesso!");
    })
    .catch(error => {
        res.status(400).send(`Erro ao gravar no Banco de Dados: ${error}`);
    });
});

//Get All
router.get('/get_all', (req, res) => {
    Product.find((error, products) => {
        if(error){
            res.status(400).send(error);
        }
        else{
            res.status(200).json(products);
        }
    });
});

//Get ID
router.get('/get/:id', (req, res) => {
    Product.findById({_id: req.params.id}, (error, product) => {
        if(error){
            res.status(400).send(error);
        }
        else{
            res.status(200).json(product);
        }
    });
});

//Update
router.put('/update/:id', (req, res) => {
    Product.findById(req.params.id, (error, product) => {
        if (!product)
            return new Error('Não foi possível carregar o documento');
        else {
            product.name = req.body.name;
            product.title = req.body.title;
            product.description = req.body.description;
  
            product.save().then(product => {
                res.status(200).send("Atualizado com sucesso!");
            })
            .catch(error => {
                res.status(400).send(`Erro ao atualizar no Banco de Dados: ${error}`);
            });
      }
    });
});

//Delete
router.delete('/delete/:id', (req, res) => {
    Product.findByIdAndRemove({_id: req.params.id}, (error, product) => {
        if(error) 
            res.status(400).send(error);
        else 
            res.status(200).send('Deletado com sucesso!');
    });
});

module.exports = router;