const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
const config = require('./src/database');
const ProductRoute = require('./src/routes/ProductRoute');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB, { useNewUrlParser: true }, error => {
    if(error) console.log(`Erro ao conectar ao banco de dados: ${error}`);
    else console.log('Banco de dados conectado!');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use('/api', ProductRoute);


const port = process.env.PORT || 4000;

app.listen(port, function(){
    console.log(`Servidor rodando na Porta: ${port}`);
});